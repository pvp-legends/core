package pvp.legends.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.plugin.Plugin;

public class MySqlInstance {

	protected Connection connection;
	protected Plugin plugin;
	private final String userName;
	private final String database;
	private final String password;
	private final String port;
	private final String hostName;

	public MySqlInstance(Plugin plugin, String hostname, String port, String database, String username,
			String password) {
		this.plugin = plugin;
		this.hostName = hostname;
		this.port = port;
		this.database = database;
		this.userName = username;
		this.password = password;
	}

	public Connection openConnection() throws ClassNotFoundException, SQLException {
		if (checkConnection())
			return this.connection;
		Class.forName("com.mysql.jdbc.Driver");
		this.connection = DriverManager.getConnection(
				"jdbc:mysql://" + this.hostName + ":" + this.port + "/" + this.database, this.userName, this.password);

		return this.connection;
	}

	public boolean checkConnection() throws SQLException {
		return (this.connection != null) && (!this.connection.isClosed());
	}

	public Connection getConnection() {
		return this.connection;
	}

	public boolean closeConnection() throws SQLException {
		if (this.connection == null)
			return false;
		this.connection.close();
		return true;
	}

	public ResultSet querySQL(String query) throws ClassNotFoundException, SQLException {
		if (!checkConnection())
			openConnection();
		Statement statement = this.connection.createStatement();
		ResultSet result = statement.executeQuery(query);
		return result;
	}

	public int updateSQL(String query) throws ClassNotFoundException, SQLException {
		if (!checkConnection())
			openConnection();
		Statement statement = this.connection.createStatement();
		int result = statement.executeUpdate(query);
		return result;
	}
}
