package pvp.legends.core.playerdata;

import java.util.HashMap;
import java.util.Map;

import pvp.legends.core.Core;
import pvp.legends.core.playerstats.StatType;
import pvp.legends.core.ranks.GameRank;
import pvp.legends.core.ranks.GlobalRank;

public class LocalPlayer {

	public Map<String, PlayerData> playerData;

	public LocalPlayer() {
		playerData = new HashMap<>();
	}

	public void createLocalPlayerData(String UUID, GlobalRank globalRank, GameRank gameRank, int time, int kills, int deaths, int souls, int points) {
		PlayerData data = new PlayerData(UUID, globalRank, gameRank, kills, deaths, souls, points, time);
		playerData.put(UUID, data);
	}

	public void reloadLocalPlayerData(String playerUUID) {
		createLocalPlayerData(playerUUID, 
				Core.getInstance().getAccountSystem().getGlobalRank(playerUUID),
				Core.getInstance().getAccountSystem().getGameRank(playerUUID),
				Core.getInstance().getActivityManager().getActivity(playerUUID),
				Core.getInstance().getPlayerStats().getPlayerStat(StatType.KILLS, playerUUID),
				Core.getInstance().getPlayerStats().getPlayerStat(StatType.DEATHS, playerUUID),
				Core.getInstance().getPlayerStats().getPlayerStat(StatType.SOULS, playerUUID),
				Core.getInstance().getPlayerStats().getPlayerStat(StatType.POINTS, playerUUID));

		if(Core.getInstance().getPlayerNickname().hasActiveNickname(playerUUID))
			Core.getLocalPlayer().getLocalPlayerData(playerUUID).setNickname(Core.getInstance().getPlayerNickname().getPlayerNickname(playerUUID));
		
		Core.getInstance().getActivityManager().startWatching(playerUUID);
	}

	public void localPlayerDisconnect(String playerUUID) {
		if (playerData.containsKey(playerUUID)) {
			PlayerData data = getLocalPlayerData(playerUUID);
			data.update();
			playerData.remove(playerUUID);
		}
		Core.getInstance().getActivityManager().stopWatching(playerUUID);
	}
	
	public void localPlayerReload(String playerUUID){
		if (playerData.containsKey(playerUUID)) {
			PlayerData data = getLocalPlayerData(playerUUID);
			data.update();
			playerData.remove(playerUUID);
		}
		Core.getInstance().getActivityManager().stopWatching(playerUUID);
	}

	public boolean localPlayerDataExists(String UUID) {
		return playerData.containsKey(UUID);
	}

	// Use this for locally editing playerData.
	public PlayerData getLocalPlayerData(String UUID) {
		return playerData.get(UUID);
	}

}
