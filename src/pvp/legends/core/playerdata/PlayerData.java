package pvp.legends.core.playerdata;

import java.text.DecimalFormat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import pvp.legends.core.Core;
import pvp.legends.core.playerstats.StatType;
import pvp.legends.core.ranks.GameRank;
import pvp.legends.core.ranks.GlobalRank;

public class PlayerData {

	private String playerUUID;
	private GlobalRank globalRank;
	private GameRank gameRank;
	private int KILLS;
	private int DEATHS;
	private int SOULS;
	private int POINTS;
	private int ONLINE_TIME;
	
	private boolean ACTIVE_NICKNAME;
	private String NICKNAME;

	public PlayerData(String UUID, GlobalRank globalRank, GameRank gameRank, int KILLS, int DEATHS, int SOULS, int POINTS, int TIME) {
		this.playerUUID = UUID;
		this.globalRank = globalRank;
		this.gameRank = gameRank;
		this.ONLINE_TIME = TIME;
		this.KILLS = KILLS;
		this.DEATHS = DEATHS;
		this.SOULS = SOULS;
		this.POINTS = POINTS;
	}

	// Update the database.
	public void update() {
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "MySQL Fetching - " + playerUUID);
		
		Core.getInstance().getAccountSystem().setRank("global", playerUUID, globalRank, null);
		Core.getInstance().getAccountSystem().setRank("game", playerUUID, null, gameRank);
		
		Core.getInstance().getPlayerStats().setPlayerStat(StatType.KILLS, playerUUID, KILLS);
		Core.getInstance().getPlayerStats().setPlayerStat(StatType.DEATHS, playerUUID, DEATHS);
		Core.getInstance().getPlayerStats().setPlayerStat(StatType.SOULS, playerUUID, SOULS);
		Core.getInstance().getPlayerStats().setPlayerStat(StatType.POINTS, playerUUID, POINTS);
		Core.getInstance().getActivityManager().setActivity(playerUUID, ONLINE_TIME);
		
		if(hasNickname()){
			if(!Core.getInstance().getPlayerNickname().hasNickname(playerUUID)){
				Core.getInstance().getPlayerNickname().addPlayer(playerUUID, NICKNAME);
			}
			Core.getInstance().getPlayerNickname().setNickname(playerUUID, NICKNAME);
		} else {
			Core.getInstance().getPlayerNickname().deleteUser(playerUUID);
		}
		
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "MySQL Success - " + ChatColor.GREEN + "Updated");
	}
	
	public boolean hasGameRank(){
		if(gameRank == GameRank.DEFAULT){
			return false;
		}
		return true;
	}

	public GlobalRank getGlobalRank() {
		return globalRank;
	}

	public void setGlobalRank(GlobalRank rank) {
		this.globalRank = rank;
	}
	
	public GameRank getGameRank() {
		return gameRank;
	}

	public void setGameRank(GameRank rank) {
		this.gameRank = rank;
	}

	public int getOnlineTime() {
		return ONLINE_TIME;
	}

	public void addOnlineTime() {
		ONLINE_TIME++;
	}

	public void setOnlineTime(int amount) {
		ONLINE_TIME = amount;
	}

	public int getKills() {
		return KILLS;
	}

	public void setKills(int amount) {
		KILLS = amount;
	}

	public void addKills(int amount) {
		KILLS = KILLS + amount;
	}

	public int getDeaths() {
		return DEATHS;
	}

	public void setDeaths(int amount) {
		DEATHS = amount;
	}

	public void addDeaths(int amount) {
		DEATHS = DEATHS + amount;
	}

	public int getSouls() {
		return SOULS;
	}

	public void setSoul(int amount) {
		SOULS = amount;
	}

	public void addSouls(int amount) {
		SOULS = SOULS + amount;
	}

	public int getPoints() {
		return POINTS;
	}

	public void setPoints(int amount) {
		POINTS = amount;
	}

	public void addPoints(int amount) {
		POINTS = POINTS + amount;
	}

	public void addKill() {
		KILLS++;
	}

	public void addDeath() {
		DEATHS++;
	}

	public String getKDR() {
		DecimalFormat df = new DecimalFormat("0.00");
		double ratio;
		if (DEATHS == 0) {
			return "0.00";
		} else {
			ratio = KILLS / (double) DEATHS;
			return df.format(ratio);
		}
	}
	
	public void setNickname(String nickname){
		ACTIVE_NICKNAME = true;
		this.NICKNAME = nickname;
	}
	
	public void activeNickname(boolean yes){
		ACTIVE_NICKNAME = yes;
	}
	
	public boolean hasNickname(){
		return ACTIVE_NICKNAME;
	}
	
	public String getNickname(){
		return NICKNAME;
	}
}
