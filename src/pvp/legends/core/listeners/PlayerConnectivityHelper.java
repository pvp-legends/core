package pvp.legends.core.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

import pvp.legends.core.Core;
import pvp.legends.core.playerstats.StatType;
import pvp.legends.core.ranks.GameRank;
import pvp.legends.core.ranks.GlobalRank;

public class PlayerConnectivityHelper implements Listener {

	@EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
	public void onSuccessfulLogin(PlayerLoginEvent e) {
		String playerUUID = e.getPlayer().getUniqueId().toString();
		String playerName = e.getPlayer().getName();
		
		if (e.getResult() == Result.ALLOWED || e.getPlayer().hasPlayedBefore()) {
			Core.getInstance().getAccountSystem().addPlayerIfMissing(playerUUID, playerName);
			Core.getInstance().getPlayerStats().addPlayerIfMissing(playerUUID, playerName);
			return;
		} else {
			Core.getLocalPlayer().createLocalPlayerData(playerUUID, GlobalRank.DEFAULT, GameRank.DEFAULT, 0, 0, 0, 0, 0);
			Core.getInstance().getAccountSystem().addPlayerIfMissing(playerUUID, playerName);
			Core.getInstance().getPlayerStats().addPlayerIfMissing(playerUUID, playerName);
		}
	}

	@EventHandler
	public void playerJoin(PlayerJoinEvent e) {
		String playerUUID = e.getPlayer().getUniqueId().toString();
		if (!Core.getLocalPlayer().localPlayerDataExists(playerUUID)) {
			Core.getLocalPlayer().createLocalPlayerData(playerUUID, 
					Core.getInstance().getAccountSystem().getGlobalRank(playerUUID),
					Core.getInstance().getAccountSystem().getGameRank(playerUUID),
					Core.getInstance().getActivityManager().getActivity(playerUUID),
					Core.getInstance().getPlayerStats().getPlayerStat(StatType.KILLS, playerUUID),
					Core.getInstance().getPlayerStats().getPlayerStat(StatType.DEATHS, playerUUID),
					Core.getInstance().getPlayerStats().getPlayerStat(StatType.SOULS, playerUUID),
					Core.getInstance().getPlayerStats().getPlayerStat(StatType.POINTS, playerUUID));
		}
		if(Core.getInstance().getPlayerNickname().hasActiveNickname(playerUUID))
			Core.getLocalPlayer().getLocalPlayerData(playerUUID).setNickname(Core.getInstance().getPlayerNickname().getPlayerNickname(playerUUID));
		//Setup Permissions
		Core.getInstance().getPlayerPermissions().setup(e.getPlayer());
		Core.getInstance().getActivityManager().startWatching(e.getPlayer().getUniqueId().toString());
	}

	@EventHandler
	public void playerLeave(PlayerQuitEvent e) {
		Core.getInstance().getPlayerPermissions().remove(e.getPlayer());
		Core.getInstance().getAccountSystem().updatePlayerName(e.getPlayer());
		Core.getLocalPlayer().localPlayerDisconnect(e.getPlayer().getUniqueId().toString());
	}
	
	@EventHandler
	public void playerBreakBlock(BlockBreakEvent e){
		if(e.getPlayer().hasPermission("tutorial.blockbreak")){
			e.setCancelled(false);
		}else
			e.setCancelled(true);
	}
}
