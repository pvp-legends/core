package pvp.legends.core.commands;

import net.md_5.bungee.api.ChatColor;
import pvp.legends.core.util.command.Command;
import pvp.legends.core.util.command.CommandArgs;

public class Ping {
	
	@Command(name = "ping", description = "Ping pong.. ping pong", usage = "/ping")
	public void helpCommand(CommandArgs args) {
		args.getSender().sendMessage(ChatColor.AQUA + "pong!");
	}

}
