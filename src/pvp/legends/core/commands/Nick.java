package pvp.legends.core.commands;

import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import pvp.legends.core.Core;
import pvp.legends.core.util.command.Command;
import pvp.legends.core.util.command.CommandArgs;

public class Nick {

	@Command(name = "nick", aliases = { "nickname" }, description = "Change your name", usage = "/nick <name>", permission = "command.nick")
	public void nickCommand(CommandArgs args) {
		if (!(args.getSender() instanceof Player)) {
			args.getSender().sendMessage(ChatColor.RED + "You are not a player!");
			return;
		}
		Player target = args.getPlayer();
		if (args.length() == 0) {
			if (Core.getLocalPlayer().getLocalPlayerData(args.getPlayer().getUniqueId().toString()).hasNickname()) {
				Core.getLocalPlayer().getLocalPlayerData(args.getPlayer().getUniqueId().toString()).activeNickname(false);
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7Your nickname has been reset!"));
				return;
			}
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &cUsage: /nick <name>"));
			return;
		} else if (args.length() > 1) {
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &cToo many arguments!"));
			return;
		}

		String nickname = ChatColor.translateAlternateColorCodes('&', args.getArgs(0));
		if (nickname == null) {
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &cPlease specify a valid nickname!"));
			return;
		}
		if (args.getSender() instanceof Player && target == ((Player) args.getSender())) {
			Core.getLocalPlayer().getLocalPlayerData(target.getUniqueId().toString()).setNickname(nickname);
			target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7You set your nickname to &r" + nickname));
		}
	}
}
