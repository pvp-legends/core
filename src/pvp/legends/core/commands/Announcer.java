package pvp.legends.core.commands;

import java.util.List;

import net.md_5.bungee.api.ChatColor;
import pvp.legends.core.Core;
import pvp.legends.core.util.command.Command;
import pvp.legends.core.util.command.CommandArgs;

public class Announcer {
	
	@Command(name = "announcer", aliases = {"an"}, description = "Edit the Announcements", usage = "/announcer", permission = "command.announcer")
	public void announcerCommand(CommandArgs args) {
		if(args.length() == 0){
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&c/announcer <enable,disable,add,remove,delay,info, ids> <int,string,id>"));
			return;
		}else if(args.length() == 1){
			if(args.getArgs(0).equalsIgnoreCase("enable")){
				if(!Core.getInstance().getAnnouncementsFile().getBoolean("enabled")){
					Core.getInstance().getAnnouncer().startBroadcaster();
					Core.getInstance().getAnnouncementsFile().set("enabled", true);
					Core.getInstance().getAnnouncementsFile().save();
				} else {
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &cAnnouncer already enabled!"));
					return;
				}
			}
			else if(args.getArgs(0).equalsIgnoreCase("disable")){
				if(Core.getInstance().getAnnouncementsFile().getBoolean("enabled")){
					Core.getInstance().getAnnouncementsFile().set("enabled", false);
					Core.getInstance().getAnnouncementsFile().save();
					Core.getInstance().getAnnouncer().stop();
				} else {
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &cAnnouncer already disabled!"));
					return;
				}
			} else if(args.getArgs(0).equalsIgnoreCase("info")){
				boolean enabled = Core.getInstance().getAnnouncementsFile().getBoolean("enabled");
				int delay = Core.getInstance().getAnnouncementsFile().getInt("delay");
				int size = Core.getInstance().getAnnouncementsFile().getStringList("messages").size();
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m=----- &a" + " &9Announcer" + " &8&m-----="));
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7Announcer Enabled: " + (enabled ? "&aTrue" : "&cFalse")));
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7Announcement Delay: " + "&e" + delay + " Seconds"));
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7Amount of Announcements: " + "&6" + size));
			}else if(args.getArgs(0).equalsIgnoreCase("ids")){
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m=----- &a" + " &9Announments" + " &8&m-----="));
				int start = 0;
				for(String announces : Core.getInstance().getAnnouncementsFile().getStringList("messages")){
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7ID #"+ start +": " + announces));
					start++;
				}
			}else {
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &cPlease specify a valid announcer option!"));
				return;
			}
		} else if(args.length() >= 2){
			if(args.getArgs(0).equalsIgnoreCase("add")){
				List<String> list = Core.getInstance().getAnnouncementsFile().getStringList("messages");
				StringBuilder str = new StringBuilder();
				for (int i = 1; i < args.length(); i++) {
					str.append(args.getArgs(i) + " ");
				}
				String message = str.toString();
				list.add(message);
				Core.getInstance().getAnnouncementsFile().set("messages", list);
				Core.getInstance().getAnnouncementsFile().save();
				Core.getInstance().getAnnouncer().updateAnnouncements();
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7Added &e" + message));
			} else if(args.getArgs(0).equalsIgnoreCase("delay")){
				int amount = Integer.parseInt(args.getArgs(1));
				Core.getInstance().getAnnouncementsFile().set("delay", amount);
				Core.getInstance().getAnnouncementsFile().save();
				Core.getInstance().getAnnouncer().updateAnnouncements();
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7Set Announcer delay to &e" + amount + " seconds"));
			}else if(args.getArgs(0).equalsIgnoreCase("remove")){
				int id = Integer.parseInt(args.getArgs(1));
				List<String> list = Core.getInstance().getAnnouncementsFile().getStringList("messages");
				String removed = list.get(id);
				list.remove(id);
				Core.getInstance().getAnnouncementsFile().set("messages", list);
				Core.getInstance().getAnnouncementsFile().save();
				Core.getInstance().getAnnouncer().updateAnnouncements();
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7Removed &r" + removed));
			}
		}
	}}
