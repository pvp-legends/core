package pvp.legends.core.commands;

import java.text.DecimalFormat;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import pvp.legends.core.Core;
import pvp.legends.core.playerstats.StatType;
import pvp.legends.core.util.command.Command;
import pvp.legends.core.util.command.CommandArgs;

public class Stats {
	
	@Command(name = "stats", aliases = { "gstats" }, description = "Shows your account statistics.", usage = "/stats")
	public void stats(CommandArgs args) {
		DecimalFormat format = new DecimalFormat("0.00");
		if (args.length() == 0) {		
			if (!(args.getSender() instanceof Player)) {				
				args.getSender().sendMessage("Usage: /stats <player>");		
				return;
			}
			Player p = args.getPlayer();
			int kills = Core.getLocalPlayer().getLocalPlayerData(p.getUniqueId().toString()).getKills();
			int deaths = Core.getLocalPlayer().getLocalPlayerData(p.getUniqueId().toString()).getDeaths();
			int time = Core.getLocalPlayer().getLocalPlayerData(p.getUniqueId().toString()).getOnlineTime();
			String kdr = Core.getLocalPlayer().getLocalPlayerData(p.getUniqueId().toString()).getKDR();
			//int points = Core.getLocalPlayer().getLocalPlayerData(p.getUniqueId().toString()).getPoints();
			//int souls= Core.getLocalPlayer().getLocalPlayerData(p.getUniqueId().toString()).getSouls();
			
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m----- &a " + args.getPlayer().getName() + " &a(Online) &8&m-----"));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Kills: &6" + kills));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Deaths: &6" + deaths));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7KDR: &6" + kdr));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cActivity: &f" + Core.getInstance().getActivityManager().formatedTime(time)));
			//args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&3Points: &f" + points));
			//args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&5&lSouls: &f" + souls));
		}
		if (args.length() == 1) {
			Player player = Bukkit.getPlayer(args.getArgs(0));
			if (player == null) {
				@SuppressWarnings("deprecation")
				OfflinePlayer op = Bukkit.getOfflinePlayer(args.getArgs(0));			
				if (op.hasPlayedBefore()) {
					int kills = Core.getInstance().getPlayerStats().getPlayerStat(StatType.KILLS, op.getUniqueId().toString());
					int deaths = Core.getInstance().getPlayerStats().getPlayerStat(StatType.DEATHS, op.getUniqueId().toString());
					int time = Core.getInstance().getActivityManager().getActivity(op.getUniqueId().toString());
					double ratio;
					String total;
					if (deaths == 0) {
						total = "0.00";
					} else {
						ratio = kills / (double) deaths;
						total = format.format(ratio);
					}
					String kdr = total;
					//int points = Core.getInstance().getPlayerStats().getPlayerStat(StatType.POINTS, op.getUniqueId().toString());
					//int souls= Core.getInstance().getPlayerStats().getPlayerStat(StatType.SOULS, op.getUniqueId().toString());
					
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m----- &a " + op.getName() + " &c(offline) &8&m-----"));
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Kills: &6" + kills));
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Deaths: &6" + deaths));
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7KDR: &6" + kdr));
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cActivity: &f" + Core.getInstance().getActivityManager().formatedTime(time)));
					//args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&3Points: &f" + points));
					//args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&5&lSouls: &f" + souls));
					return;
				}
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThis user was not found in the database."));
				return;
			}		
			int kills = Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).getKills();
			int deaths = Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).getDeaths();
			int time = Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).getOnlineTime();
			String kdr = Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).getKDR();
			//int points = Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).getPoints();
			//int souls= Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).getSouls();
			
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m----- &a " + player.getName() + " &c(offline) &8&m-----"));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Kills: &6" + kills));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Deaths: &6" + deaths));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7KDR: &6" + kdr));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cActivity: &f" + Core.getInstance().getActivityManager().formatedTime(time)));
			//args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&3Points: &f" + points));
			//args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&5&lSouls: &f" + souls));
			return;

		}
	}

}
