package pvp.legends.core.commands;

import pvp.legends.core.Core;
import pvp.legends.core.util.command.Command;
import pvp.legends.core.util.command.CommandArgs;

public class Help {
	
	@Command(name = "help", description = "Displays the help information", usage = "/help")
	public void helpCommand(CommandArgs args) {
		Core.getInstance().getCommandFramework().messageCommandInformation(args.getSender());
	}
}
