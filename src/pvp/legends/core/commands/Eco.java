package pvp.legends.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import pvp.legends.core.Core;
import pvp.legends.core.playerstats.StatType;
import pvp.legends.core.util.command.Command;
import pvp.legends.core.util.command.CommandArgs;

public class Eco {
	
	/*
	 *  Usage: /eco <set,give> <playername> <point,souls> <amount>
	 *  
	 *  Example:
	 *  	/eco give points 100 - Gives yourself points.
	 *  	/eco give mike1665 points 100 - Gives mike1665 100 points.
	 * 
	 */
	
	@Command(name = "eco", description = "Admin command", usage = "/eco", permission = "command.eco")
	public void ecoCommant(CommandArgs args){
		if(args.length() == 0){
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m----- &a " + "&6Competitive&cEco" + " &8&m-----"));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/eco <give/set> <player> <points/souls> <amount>"));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/eco <give/set> <points/souls> <amount>"));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&aExample giving someone 100 points: "));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&c/eco give mike1665 points 100"));
		}
		if(args.length() == 4){			
			//Set
			if(args.getArgs(0).equalsIgnoreCase("set")){
				Player target = Bukkit.getPlayer(args.getArgs(1));
				//Offline or does not exist...
				if(target == null){
					@SuppressWarnings("deprecation")
					OfflinePlayer op = Bukkit.getOfflinePlayer(args.getArgs(1));
					if(Core.getInstance().getAccountSystem().playerExists(op.getUniqueId().toString())){
						StatType type = StatType.valueOf(args.getArgs(2));
						if(type != null){
							String amount = args.getArgs(3);
							Core.getInstance().getPlayerStats().setPlayerStat(type, op.getUniqueId().toString(), Integer.parseInt(amount));
						}

					} else {
						args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &cThis user was not found in the database."));
						return;
					}
				}
				if(args.getArgs(2).equalsIgnoreCase("points")){
					String amount = args.getArgs(3);
					Core.getLocalPlayer().getLocalPlayerData(target.getUniqueId().toString()).setPoints(Integer.parseInt(amount));
					//args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7You set &e" + target.getName() + " &7souls to: &5&l" + amount + " souls"));
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7You set &e" + target.getName() + " &7souls to: &5&l" + amount + " souls"));				
				}else if(args.getArgs(2).equalsIgnoreCase("souls")){
					String amount = args.getArgs(3);
					Core.getLocalPlayer().getLocalPlayerData(target.getUniqueId().toString()).setSoul(Integer.parseInt(amount));
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7You set &e" + target.getName() + " &7souls to: &5&l" + amount + " souls"));
				}
			}
			//Give
			else if(args.getArgs(0).equalsIgnoreCase("give")){
				Player target = Bukkit.getPlayer(args.getArgs(1));
				//Offline or does not exist...
				if(target == null){
					@SuppressWarnings("deprecation")
					OfflinePlayer op = Bukkit.getOfflinePlayer(args.getArgs(1));
					if(Core.getInstance().getAccountSystem().playerExists(op.getUniqueId().toString())){
						StatType type = StatType.valueOf(args.getArgs(2));
						if(type != null){
							String amount = args.getArgs(3);
							Core.getInstance().getPlayerStats().addToStat(type, op.getUniqueId().toString(), Integer.parseInt(amount));
						}
					} else {
						args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &cThis user was not found in the database."));
						return;
					}
				}
				if(args.getArgs(2).equalsIgnoreCase("points")){
					String amount = args.getArgs(3);
					Core.getLocalPlayer().getLocalPlayerData(target.getUniqueId().toString()).addPoints(Integer.parseInt(amount));
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7You gave &e" + target.getName() + " &7points to: &3" + amount + " points"));
				}else if(args.getArgs(2).equalsIgnoreCase("souls")){
					String amount = args.getArgs(3);
					Core.getLocalPlayer().getLocalPlayerData(target.getUniqueId().toString()).addSouls(Integer.parseInt(amount));
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7You gave &e" + target.getName() + " &7souls to: &5&l" + amount + " souls"));
				}
			}
		//YOUR SELF
		} if (args.length() == 3){
			Player player = args.getPlayer();
			String amount = args.getArgs(2);
			if(args.getArgs(0).equalsIgnoreCase("set")){
				if(args.getArgs(1).equalsIgnoreCase("points")){
					Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).setPoints(Integer.parseInt(amount));
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7You set your points to: &3" + amount));
				}
				else if(args.getArgs(1).equalsIgnoreCase("souls")){
					Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).setSoul(Integer.parseInt(amount));
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7You set your souls to: &5&l" + amount));
				}
			}
			else if(args.getArgs(0).equalsIgnoreCase("give")){
				if(args.getArgs(1).equalsIgnoreCase("points")){
					Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).addPoints(Integer.parseInt(amount));
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7You gave yourself: &3" + amount + " points"));
					
				}
				else if(args.getArgs(1).equalsIgnoreCase("souls")){
					Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).addSouls(Integer.parseInt(amount));
					args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &7You gave yourself: &5&l" + amount + " souls"));
				}
			}
		}
	}
}
