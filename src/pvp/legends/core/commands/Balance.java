package pvp.legends.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import pvp.legends.core.Core;
import pvp.legends.core.util.command.Command;
import pvp.legends.core.util.command.CommandArgs;

public class Balance {

	@SuppressWarnings("deprecation")
	@Command(name = "balance", aliases = { "bal", "points", "money", "souls","coins" }, description = "Shows your server balance.", usage = "/balance")
	public void balanceCommand(CommandArgs args) {
		if (args.length() != 1) {
			if (!(args.getSender() instanceof Player)) {
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou are not a player!"));
				return;
			}
			Player p = args.getPlayer();
			int points = Core.getLocalPlayer().getLocalPlayerData(p.getUniqueId().toString()).getPoints();
			int souls = Core.getLocalPlayer().getLocalPlayerData(p.getUniqueId().toString()).getSouls();
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m----- &a " + args.getPlayer().getName() + " &a(Online) &8&m-----"));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&3Points: &f" + points));
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&5&lSouls: &f" + souls));
			return;
		}
		Player target = Bukkit.getPlayer(args.getArgs(0));
		if (target == null) {
			String check = args.getArgs(0);
			OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args.getArgs(0));
			if (!offlinePlayer.hasPlayedBefore()) {
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &c" + args.getArgs(0) + " has never played before!"));
				return;
			}
			if(Core.getInstance().getPlayerStats().playerExists(offlinePlayer.getUniqueId().toString())){
				int points = Core.getLocalPlayer().getLocalPlayerData(offlinePlayer.getUniqueId().toString()).getPoints();
				int souls = Core.getLocalPlayer().getLocalPlayerData(offlinePlayer.getUniqueId().toString()).getSouls();
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m----- &a " + offlinePlayer.getName() + " &c(offline) &8&m-----"));
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&3Points: &f" + points));
				args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&5&lSouls: &f" + souls));
			}
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &cCouldn't find profile of " + check + "!"));
			return;
		}
		int points = Core.getLocalPlayer().getLocalPlayerData(target.getUniqueId().toString()).getPoints();
		int souls = Core.getLocalPlayer().getLocalPlayerData(target.getUniqueId().toString()).getSouls();
		args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m----- &a " + target.getPlayer().getName() + " &a(Online) &8&m-----"));
		args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&3Points: &f" + points));
		args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&5&lSouls: &f" + souls));
	}
}
