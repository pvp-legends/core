package pvp.legends.core.commands;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import pvp.legends.core.util.command.Command;
import pvp.legends.core.util.command.CommandArgs;

public class Me {
	
	@Command(name = "me", description = "Express yourself", usage = "/me {message}", permission = "command.me")
	public void nickCommand(CommandArgs args) {
		if (!(args.getSender() instanceof Player)) {
			args.getSender().sendMessage(ChatColor.RED + "You are not a player!");
			return;
		}
		if(args.length() == 0){
			args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &cUsage: /me <message>"));	
			return;
		}
		Player target = args.getPlayer();
		if (target.hasPermission("command.me")) {
			StringBuilder str = new StringBuilder();
			for (int i = 0; i < args.length(); i++) {
				str.append(args.getArgs(i) + " ");
			}
			String message = str.toString();
			Bukkit.getServer().broadcastMessage(ChatColor.YELLOW + "" + ChatColor.BOLD + "* " + ChatColor.RESET + ChatColor.YELLOW + target.getName() + ChatColor.DARK_GRAY + "> " + ChatColor.WHITE + message);
		} else {
			target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &cYou need premium!"));
			return;
		}
	}

}
