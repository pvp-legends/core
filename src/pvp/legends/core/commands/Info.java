package pvp.legends.core.commands;

import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginDescriptionFile;

import pvp.legends.core.Core;
import pvp.legends.core.util.command.Command;
import pvp.legends.core.util.command.CommandArgs;

public class Info {
	
	@Command(name = "info", description = "Server information", usage = "/info")
	public void helpCommand(CommandArgs args) {
		PluginDescriptionFile pdf = Core.getInstance().getDescription(); //Gets plugin.yml
		args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &eThis server is running &aCore v" + pdf.getVersion()));
		args.getSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8� &eDeveloped by: &cmike1665"));
	}

}
