package pvp.legends.core.account;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import pvp.legends.core.Core;
import pvp.legends.core.ranks.GameRank;
import pvp.legends.core.ranks.GlobalRank;

public class AccountSystem {
	
	public AccountSystem() {}
	
	public boolean playerExists(String playerUUID) {
		try {
			ResultSet result = Core.getSql().querySQL("SELECT * FROM Account WHERE UUID = '" + playerUUID + "';");
			if(!result.next())
				return false;
			return true;
		} catch(SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void addPlayer(String playerUUID, String playerName) {
		try {
			ResultSet result = Core.getSql().querySQL("SELECT * FROM Account WHERE UUID = '" + playerUUID + "';");
			if(!result.next())
				Core.getSql().updateSQL("INSERT INTO Account (ID, UUID, PlayerName, GlobalRank, GameRank, Activity) VALUES (NULL, '" + playerUUID + "', '" + playerName + "', DEFAULT, DEFAULT, '0');");
			
			ResultSet result2 = Core.getSql().querySQL("SELECT * FROM Information WHERE UUID = '" + playerUUID + "';");
			if(!result2.next())
				Core.getSql().updateSQL("INSERT INTO Information (Name, UUID) VALUES ('" + playerName + "', '" + playerUUID + "');");
		
		}catch(SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void updatePlayerName(Player player){
		try {
			Core.getSql().updateSQL("UPDATE Account SET PlayerName = '" + player.getName() + "' WHERE UUID = '" + player.getUniqueId().toString() + "';");
			Core.getSql().updateSQL("UPDATE Information SET Name = '" + player.getName() + "' WHERE UUID = '" + player.getUniqueId().toString() + "';");
		} catch(SQLException|ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public String getUUID(String playerName) {
		try {
			ResultSet result = Core.getSql().querySQL("SELECT * FROM Information WHERE Name = '" + playerName + "';");
			if(!result.next())
				return null;
			return result.getString("UUID");
		} catch(SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void addPlayerIfMissing(final String playerUUID, final String playerName) {
		Bukkit.getScheduler().runTaskAsynchronously(Core.getInstance(), new Runnable() {
			@Override
			public void run() {
				if(!playerExists(playerUUID))
					addPlayer(playerUUID, playerName);
			}
		});
	}
	
	public GlobalRank getGlobalRank(String playerUUID) {
		try {
			ResultSet result = Core.getSql().querySQL("SELECT * FROM Account WHERE UUID = '" + playerUUID + "';");
			if(!result.next())
				return GlobalRank.DEFAULT;
			String rank = result.getString("GlobalRank");
			return GlobalRank.valueOf(rank);
		} catch (SQLException|ClassNotFoundException e) {
			e.printStackTrace();
		}
		return GlobalRank.DEFAULT;
		
	}
	
	public GameRank getGameRank(String playerUUID) {
		try {
			ResultSet result = Core.getSql().querySQL("SELECT * FROM Account WHERE UUID = '" + playerUUID + "';");
			if(!result.next())
				return GameRank.DEFAULT;
			String rank = result.getString("GameRank");
			return GameRank.valueOf(rank);
		} catch (SQLException|ClassNotFoundException e) {
			e.printStackTrace();
		}
		return GameRank.DEFAULT;
		
	}
	
	public void setRank(String type, String UUID, GlobalRank globalRank, GameRank gameRank) {
		if(type.equalsIgnoreCase("game")){
			String ranks = gameRank.toString();
			try {
				Core.getSql().updateSQL("UPDATE Account SET GameRank = '" + ranks + "' WHERE UUID = '" + UUID + "';");
			} catch(SQLException|ClassNotFoundException e) {
				e.printStackTrace();
			}
		}else if(type.equalsIgnoreCase("global")){
			String ranks = globalRank.toString();
			try {
				Core.getSql().updateSQL("UPDATE Account SET GlobalRank = '" + ranks + "' WHERE UUID = '" + UUID + "';");
			} catch(SQLException|ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
