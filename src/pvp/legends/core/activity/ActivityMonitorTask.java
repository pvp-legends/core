package pvp.legends.core.activity;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import pvp.legends.core.Core;

public class ActivityMonitorTask extends BukkitRunnable{

	String player;
	private int seconds = 60;

	public ActivityMonitorTask(String id) {
		this.player = id;
	}

	public void start() {
		this.runTaskTimer(Core.getInstance(), 20 * seconds, 20 * seconds);
	}

	@Override
	public void run() {
		Bukkit.getServer().getLogger().info(player + " added 1 to activity.");
		Core.getLocalPlayer().getLocalPlayerData(player).addOnlineTime();
	}

}
