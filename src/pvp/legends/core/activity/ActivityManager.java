package pvp.legends.core.activity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.bukkit.ChatColor;

import pvp.legends.core.Core;

public class ActivityManager {

	Map<String, ActivityMonitorTask> running_tasks = new HashMap<>();

	public ActivityManager() {}

	public int getActivity(String UUID) {
		try {
			ResultSet res = Core.getSql().querySQL("SELECT * FROM Account WHERE UUID = '" + UUID + "';");
			if (!res.next())
				return 0;
			return res.getInt("Activity");
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void addActivity(String UUID) {
		try {
			int last_time = this.getActivity(UUID);
			int updated_time = last_time + 1;
			Core.getSql().updateSQL("UPDATE Account SET Activity = '" + updated_time + "' WHERE UUID = '" + UUID + "';");
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void setActivity(String UUID, int time) {
		try {
			Core.getSql().updateSQL("UPDATE Account SET Activity = '" + time + "' WHERE UUID = '" + UUID + "';");
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void startWatching(String playerUUID) {
		this.running_tasks.put(playerUUID, new ActivityMonitorTask(playerUUID));
		this.running_tasks.get(playerUUID).start();
	}

	public void stopWatching(String playerUUID) {
		if (this.running_tasks.containsKey(playerUUID)) {
			this.running_tasks.get(playerUUID).cancel();
			this.running_tasks.remove(playerUUID);
		}
	}

	public String formatedTime(int time) {
		int seconds = time * 60;
		int day = (int) TimeUnit.SECONDS.toDays(seconds);
		long hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
		long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);

		return ChatColor.translateAlternateColorCodes('&', "&6" + day + " &eDays&f, &6" + hours + " &eHours&f, &6" + minute + " &eMinutes");
		//return ChatColor.GREEN + "" + "DAYS: " + ChatColor.WHITE + day + " || " + ChatColor.GREEN + "" + "Hours: " + ChatColor.WHITE + hours + " || " + ChatColor.GREEN + "" +  "Mins: " +  minute;
	}

}
