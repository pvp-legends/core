package pvp.legends.core.permissions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pvp.legends.core.Core;

public class SetupPermissions {

	private HashMap<String, List<String>> localPermissions;

	public SetupPermissions() {
		localPermissions = new HashMap<>();
		for (String group : Core.getInstance().getPermissionsFile().getSection("Groups").getKeys(false)) {
			List<String> gPerms = Core.getInstance().getPermissionsFile().getStringList("Groups." + group + ".permissions");
			localPermissions.put(group, gPerms);
		}
	}

	public String getRankFromPermission(String permission){
		return (String) getKeyFromValue(localPermissions, permission);
	}
	
	public List<String> getPermissionsFromRank(String rank){
		return localPermissions.get(rank);
	}
	
	public void clearPermissions(){
		localPermissions.clear();
	}

	public static Object getKeyFromValue(Map<String, List<String>> hm, Object value) {
		for (Object o : hm.keySet()) {
			if (hm.get(o).equals(value)) {
				return o;
			}
		}
		return null;
	}

}
