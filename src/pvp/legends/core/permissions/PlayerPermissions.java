package pvp.legends.core.permissions;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import pvp.legends.core.Core;

public class PlayerPermissions {

	public PlayerPermissions() {
	}

	public HashMap<UUID, PermissionAttachment> playerPermissions = new HashMap<>();

	public void setup(Player player) {
		if (player == null)
			return;

		PermissionAttachment attachment = player.addAttachment(Core.getInstance());
		this.playerPermissions.put(player.getUniqueId(), attachment);
		String rank = Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).getGlobalRank().toString().toLowerCase();
		for (String perms : Core.getInstance().getPermissionsSetup().getPermissionsFromRank(rank)) {
			if (perms.contains("-")) {
				String delPerm = removeCharAt(perms, 0);
				System.out.println("Minus " + delPerm);
				
				attachment.setPermission(delPerm, false);
				//attachment.unsetPermission(delPerm);
			}else {
				System.out.println(perms);
				attachment.setPermission(perms, true);
			}
		}
	}

	public void remove(Player player) {
		playerPermissions.remove(player.getUniqueId());
	}

	public void clear() {
		this.playerPermissions.clear();
	}

	public String removeCharAt(String s, int pos) {
		return s.substring(0, pos) + s.substring(pos + 1);
	}
}
