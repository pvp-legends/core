package pvp.legends.core.playerstats;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;

import pvp.legends.core.Core;

public class StatsManager {

	public StatsManager() { }

	public boolean playerExists(String playerUUID) {
		try {
			ResultSet result = Core.getSql()
					.querySQL("SELECT * FROM Currency WHERE PlayerUUID = '" + playerUUID + "';");
			if (!result.next())
				return false;
			return true;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void addPlayer(String playerUUID, String playerName) {
		try {
			ResultSet result = Core.getSql()
					.querySQL("SELECT * FROM Currency WHERE PlayerUUID = '" + playerUUID + "';");
			;
			if (!result.next())
				Core.getSql().updateSQL(
						"INSERT INTO Currency (ID, PlayerUUID, PlayerName, Kills, Deaths, Souls, Points) VALUES (NULL, '"
								+ playerUUID + "', '" + playerName + "', '0', '0', '0', '0');");
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void addPlayerIfMissing(final String playerUUID, final String name) {
		Bukkit.getScheduler().runTaskAsynchronously(Core.getInstance(), new Runnable() {

			@Override
			public void run() {
				if (!playerExists(playerUUID)) {
					addPlayer(playerUUID, name);
				}
			}
		});
	}
	
	public int getPlayerStat(StatType type, String playerUUID) {
		try {
			String resultType = type.getDBTitle();
			ResultSet result = Core.getSql()
					.querySQL("SELECT * FROM Currency WHERE PlayerUUID = '" + playerUUID + "';");
			boolean hasNext = result.next();
			if (!hasNext) {
				return 0;
			}
			return result.getInt(resultType);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void setPlayerStat(StatType type, String playerUUID, int amount) {
		try {
			Core.getSql().updateSQL("UPDATE Currency SET " + type.getDBTitle() + " = '" + amount
					+ "' WHERE PlayerUUID = '" + playerUUID + "';");
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	public void addToStat(StatType type, String playerUUID, int amount){
		try {
			Core.getSql().updateSQL("UPDATE Currency SET " + type.getDBTitle() + " = '" + (getPlayerStat(type, playerUUID) + amount) + "' WHERE PlayerUUID = '" + playerUUID + "';");
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
