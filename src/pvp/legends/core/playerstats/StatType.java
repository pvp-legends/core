package pvp.legends.core.playerstats;

public enum StatType {
	KILLS("Kills"), 
	DEATHS("Deaths"), 
	POINTS("Points"), 
	SOULS("Souls");

	String dbTitle;

	StatType(String dbTitle) {
		this.dbTitle = dbTitle;
	}

	public String getDBTitle() {
		return dbTitle;
	}
}
