package pvp.legends.core.nickname;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;

import pvp.legends.core.Core;

public class PlayerNickname {
	
	public PlayerNickname() { }
	
	public boolean hasNickname(String playerUUID) {
		try {
			ResultSet result = Core.getSql().querySQL("SELECT * FROM nickname WHERE PlayerUUID = '" + playerUUID + "';");
			if(!result.next())
				return false;
			return true;
		} catch(SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean hasActiveNickname(String playerUUID) {
		try {
			ResultSet result = Core.getSql().querySQL("SELECT * FROM nickname WHERE PlayerUUID = '" + playerUUID + "';");
			boolean hasactive = result.next();
			if(!hasactive)
				return false;
			return result.getBoolean("Active");
		} catch(SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void addPlayer(final String playerUUID, final String playerNick) {
		Bukkit.getScheduler().runTaskAsynchronously(Core.getInstance(), new Runnable() {	
			@Override
			public void run() {
				try {
					ResultSet result = Core.getSql().querySQL("SELECT * FROM nickname WHERE PlayerUUID = '" + playerUUID + "';");
					if (!result.next())
						Core.getSql().updateSQL("INSERT INTO nickname (PlayerUUID, Nickname, Active) VALUES ('" + playerUUID + "', '" + playerNick + "', true);");
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public String getPlayerNickname(String playerUUID) {
		try {
			ResultSet result = Core.getSql().querySQL("SELECT * FROM nickname WHERE PlayerUUID = '" + playerUUID + "';");
			boolean hasNext = result.next();
			if (!hasNext) {
				return null;
			}
			return result.getString("Nickname");
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void setNickname(String UUID, String nickname) {
		try {
			Core.getSql().updateSQL("UPDATE nickname SET Nickname = '" + nickname + "' WHERE PlayerUUID = '" + UUID + "';");
		} catch(SQLException|ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteUser(String UUID) {
		try {
			Core.getSql().updateSQL("DELETE FROM nickname WHERE PlayerUUID = '" + UUID + "';");
		} catch(SQLException|ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
