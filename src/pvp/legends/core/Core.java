package pvp.legends.core;

import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import net.md_5.bungee.api.ChatColor;
import pvp.legends.core.account.AccountSystem;
import pvp.legends.core.activity.ActivityManager;
import pvp.legends.core.announcer.Announcer;
import pvp.legends.core.commands.Balance;
import pvp.legends.core.commands.Eco;
import pvp.legends.core.commands.Help;
import pvp.legends.core.commands.Info;
import pvp.legends.core.commands.Me;
import pvp.legends.core.commands.Nick;
import pvp.legends.core.commands.Ping;
import pvp.legends.core.commands.Stats;
import pvp.legends.core.listeners.PlayerConnectivityHelper;
import pvp.legends.core.nickname.PlayerNickname;
import pvp.legends.core.permissions.PlayerPermissions;
import pvp.legends.core.permissions.SetupPermissions;
import pvp.legends.core.playerdata.LocalPlayer;
import pvp.legends.core.playerdata.NetworkPlayer;
import pvp.legends.core.playerstats.StatsManager;
import pvp.legends.core.ranks.RankChat;
import pvp.legends.core.util.ConfigManager;
import pvp.legends.core.util.command.CommandFramework;
import pvp.legends.sql.MySqlInstance;

public class Core extends JavaPlugin {

	public static MySqlInstance mysql;
	public static NetworkPlayer networkPlayer;
	public static LocalPlayer localPlayer;
	public static BukkitTask tpsMonitor;

	private AccountSystem accountSystem;
	private ActivityManager activityManager;
	private CommandFramework framework;
	private StatsManager playerStats;
	private PlayerNickname nickname;
	private ConfigManager permissions, announcements;
	private SetupPermissions setupPermissions;
	private PlayerPermissions playerPermissions;
	Announcer announcer;
	
	static Core instance;

	@Override
	public void onEnable() {
		instance = this;
		long startTime = System.nanoTime();
		
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "[MySql]" + ChatColor.YELLOW + " Establishing connection... ");
		mysql = new MySqlInstance(this, "localhost", "3306", "core", "root", "pvplegends");
		try {
			mysql.openConnection();
		} catch (ClassNotFoundException | SQLException e) {
			Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "[MySql]" + ChatColor.DARK_RED + " Failed to connect to database! Shutting down...");
			Bukkit.getServer().shutdown();
		}
		
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "[Server]" + ChatColor.YELLOW + " Loading Configs... ");
		permissions = new ConfigManager("permissions");
		announcements = new ConfigManager("announcements");	
		
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "[Server]" + ChatColor.YELLOW + " Loading Classes... ");
		framework = new CommandFramework(this);	
		accountSystem = new AccountSystem();
		activityManager = new ActivityManager();
		localPlayer = new LocalPlayer();
		networkPlayer = new NetworkPlayer();
		playerStats = new StatsManager();
		nickname = new PlayerNickname();

		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "[Server]" + ChatColor.YELLOW + " Setting local permissions... ");
		setupPermissions = new SetupPermissions();
		playerPermissions = new PlayerPermissions();
		
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "[Server]" + ChatColor.YELLOW + " Registring Events... ");
		loadListeners(new RankChat(), new PlayerConnectivityHelper());
		
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "[Server]" + ChatColor.YELLOW + " Loading Commands... ");
		loadCommands(new Nick(), new Stats(), new Balance(), new Eco(), new Me(), new Info(), new pvp.legends.core.commands.Announcer(), new Ping(), new Help());
	
		//Reload information of players that are online upon a reload.
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			if(!Core.getLocalPlayer().localPlayerDataExists(player.getUniqueId().toString()))
				Core.getLocalPlayer().reloadLocalPlayerData(player.getUniqueId().toString());
			playerPermissions.setup(player);
		}
		
		long finishTime = System.nanoTime();
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "[Server]" + ChatColor.GREEN + " Finished in - " + TimeUnit.NANOSECONDS.toMillis(finishTime - startTime) + " ms" );
		
		announcer = new Announcer(this);
	}

	@Override
	public void onDisable() {
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			if(Core.getLocalPlayer().localPlayerDataExists(player.getUniqueId().toString()))
				Core.getLocalPlayer().localPlayerReload(player.getUniqueId().toString());
		}
		setupPermissions.clearPermissions();

		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "[MySql]" + ChatColor.YELLOW + " Disabling Managers... ");
		setupPermissions = null;
		playerPermissions = null;
		framework = null;
		accountSystem = null;
		activityManager = null;
		localPlayer = null;
		networkPlayer = null;
		playerStats = null;
		nickname = null;
		permissions = null;
	
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "[MySql]" + ChatColor.YELLOW + " Disabling MySql... ");
		try {
			mysql.closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		instance = null;
	}

	public static Core getInstance() {
		return instance;
	}

	public AccountSystem getAccountSystem() {
		return accountSystem;
	}

	public ActivityManager getActivityManager() {
		return activityManager;
	}

	public NetworkPlayer getNetworkPlayer(){
		return networkPlayer;
	}
	
	public static LocalPlayer getLocalPlayer() {
		return localPlayer;
	}

	public StatsManager getPlayerStats() {
		return playerStats;
	}

	public PlayerNickname getPlayerNickname() {
		return nickname;
	}

	public CommandFramework getCommandFramework() {
		return framework;
	}
	
	public SetupPermissions getPermissionsSetup(){
		return setupPermissions;
	}
	
	public PlayerPermissions getPlayerPermissions(){
		return playerPermissions;
	}
	
	public static MySqlInstance getSql() {
		return mysql;
	}
	
	public Announcer getAnnouncer(){
		return announcer;
	}
	
	/* Configuration */
	public ConfigManager getPermissionsFile(){
		return permissions;
	}
	
	public ConfigManager getAnnouncementsFile(){
		return announcements;
	}

	private void loadCommands(Object... objects) {
		for (Object object : objects) {
			framework.registerCommands(object);
		}
	}
	
	private void loadListeners(Listener... listeners) {
		for (Listener l : listeners) {
			Bukkit.getPluginManager().registerEvents(l, this);
		}
	}
}
