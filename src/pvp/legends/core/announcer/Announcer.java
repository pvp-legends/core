package pvp.legends.core.announcer;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import pvp.legends.core.Core;

public class Announcer {

	private BukkitTask announceTask;
	private RunAnnouncer announcer;
	private long delay;
	private Core plugin;
	public boolean enabled;

	public Announcer(Core plugin) {
		this.plugin = plugin;
		this.announcer = new RunAnnouncer();
		startBroadcaster();
	}

	public void startBroadcaster() {
		this.delay = (Core.getInstance().getAnnouncementsFile().getInt("delay") * 20L);
		this.enabled = Core.getInstance().getAnnouncementsFile().getBoolean("enabled");
		
		if (enabled) {
			announceTask = Bukkit.getScheduler().runTaskTimer(this.plugin, announcer, this.delay, this.delay);
		}
	}

	public void stop() {
		this.enabled = false;
		announceTask.cancel();
	}
	
	public void updateAnnouncements(){
		stop();
		this.announcer.configMessages = Core.getInstance().getAnnouncementsFile().getStringList("messages");
		this.announcer.size = this.announcer.configMessages.size();
		this.announcer.permSize = this.announcer.configMessages.size();
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), new Runnable() {		
			@Override
			public void run() {
				startBroadcaster();
		}
		}, 20L);
	}
}
