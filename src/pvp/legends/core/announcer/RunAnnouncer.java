package pvp.legends.core.announcer;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import pvp.legends.core.Core;

public class RunAnnouncer implements Runnable {

	public List<String> configMessages = Core.getInstance().getAnnouncementsFile().getStringList("messages");
	public int size;
	public int permSize;

	public RunAnnouncer() {
		this.size = this.configMessages.size();
		this.permSize = this.configMessages.size();
	}

	@Override
	public void run() {
		if (this.size == 0) {
			this.size = this.permSize;
		}
		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&e&l(!) &r&8� &r" + this.configMessages.get(this.size - 1)));
		this.size -= 1;
	}
}
