package pvp.legends.core.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import pvp.legends.core.Core;

public class SqlHelper {

	public static boolean sqlPlayerExists(String database, String playerUUID) {
		try {
			ResultSet result = Core.getSql()
					.querySQL("SELECT * FROM " + database + " WHERE PlayerUUID = '" + playerUUID + "';");
			if (!result.next())
				return false;
			return true;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}
}
