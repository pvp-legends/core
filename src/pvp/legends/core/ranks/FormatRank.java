package pvp.legends.core.ranks;

import org.bukkit.entity.Player;

import pvp.legends.core.Core;

public class FormatRank {
	
	public static String formatRank(Player player){
		GlobalRank globalRank = Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).getGlobalRank();
		GameRank gameRank = Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).getGameRank();
		if(Core.getLocalPlayer().getLocalPlayerData(player.getUniqueId().toString()).hasGameRank()){
			return globalRank.getFormat() + " " + gameRank.getFormat();
		}else {
			return globalRank.getFormat();
		}
	}
}
