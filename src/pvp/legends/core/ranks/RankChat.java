package pvp.legends.core.ranks;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import pvp.legends.core.Core;

public class RankChat implements Listener {

	@EventHandler
	public void chatFormatMessage(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		boolean hasNickname = false;;
		if (Core.getLocalPlayer().getLocalPlayerData(p.getUniqueId().toString()).hasNickname()) 
			hasNickname = true;
		
		if(p.hasPermission("color.message") && hasNickname){
			String nickname = Core.getLocalPlayer().getLocalPlayerData(p.getUniqueId().toString()).getNickname();
			e.setFormat(FormatRank.formatRank(p) + ChatColor.RESET + " " + ChatColor.GRAY + nickname + ChatColor.DARK_GRAY + ": " + ChatColor.RESET + ChatColor.translateAlternateColorCodes('&', e.getMessage()));
		} else if(hasNickname){
			String nickname = Core.getLocalPlayer().getLocalPlayerData(p.getUniqueId().toString()).getNickname();
			e.setFormat(FormatRank.formatRank(p) +ChatColor.RESET + " " + ChatColor.GRAY + nickname + ChatColor.DARK_GRAY + ": " + ChatColor.RESET + e.getMessage());
		}else {
			String message = p.hasPermission("color.message") ? ChatColor.translateAlternateColorCodes('&', e.getMessage()): e.getMessage();
			e.setFormat(FormatRank.formatRank(p) + ChatColor.RESET + " "+ ChatColor.GRAY + p.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.RESET + message);

		}
	}
}
