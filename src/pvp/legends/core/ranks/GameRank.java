package pvp.legends.core.ranks;

import org.bukkit.ChatColor;

public enum GameRank {
	DEFAULT(ChatColor.WHITE + ""),
	GURU(ChatColor.BLUE + "Guru"),
	HERO(ChatColor.YELLOW + "Hero"),
	WARLORD(ChatColor.RED + "Warlord");
	
	private String format;
	
	GameRank(String format){
		this.format = format;
	}
	
	public String getFormat(){
		return format;
	}
}
