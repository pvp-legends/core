package pvp.legends.core.ranks;

import org.bukkit.ChatColor;

public enum GlobalRank {
	DEFAULT(ChatColor.GRAY + "Default"), 
	PREMIUM(ChatColor.AQUA + "Premium"), 
	ULTIMATE(ChatColor.YELLOW + "" + ChatColor.BOLD + "Ultimate"), 
	PLATINUM(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Platinum"), 
	HELPER(ChatColor.GREEN + "Helper"),
	MOD(ChatColor.AQUA + "Mod"), 
	ADMIN(ChatColor.RED + "Admin"), 
	DEVELOPER(ChatColor.DARK_PURPLE + "Dev"), 
	OWNER(ChatColor.GOLD + "" + ChatColor.BOLD + "Owner");

	private String format;

	GlobalRank(String format) {
		this.format = format;
	}

	public String getFormat() {
		return format;
	}
}
